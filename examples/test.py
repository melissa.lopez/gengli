"""
A bunch of units tests to be performed.
To run them:

	pytest test.py

They are added as part of the CI pipeline in gitlab
"""

import sys
sys.path.insert(0, '..')

def test_import():
	import gengli
	from gengli import glitch_generator
	from gengli.utils import metrics, compute_distance_matrix
	from gengli.ctgan import model
	return True

def test_basics():
	import gengli
	g = gengli.glitch_generator('H1')
	g.get_raw_glitch()
	g.get_glitch()
	return True

def test_snr():
	"Testing that the snr computed by the code agrees with pycbc"
	import gengli
	import numpy as np
	from gengli import glitch_generator				
	import pycbc.filter
	import pycbc.types
	
	g = glitch_generator('H1')
	diff_list = []
	for _ in range(100):
		srate = np.random.uniform(2048., 4096.*4)
		my_snr = np.random.uniform(10, 100)
		
		glitch = g.get_glitch(1, snr = my_snr, srate = srate)
		glitch_pycbc = pycbc.types.timeseries.TimeSeries(glitch,
				delta_t = 1./srate, dtype = np.float64)
		snr_pycbc = pycbc.filter.sigmasq(glitch_pycbc)
		#print("SNR mine | pycbc ",my_snr, np.sqrt(snr_pycbc))
		diff_list.append((my_snr - np.sqrt(snr_pycbc))/np.sqrt(snr_pycbc))
	mean_diff = np.mean(diff_list)
	std_diff = np.std(diff_list)
	
		#checking that the mean difference is zero within a sigma
	assert np.abs(mean_diff)<std_diff, "The discrepancy between the SNR set by gengli and the one computed by pycbc is too large!"
	return True

def test_confidence_interval():
	import gengli
	import time
	N_glitches = 10
	g = gengli.glitch_generator('L1')
	s = time.time()
	g.initialize_benchmark_set(100)
	print("Benchmark set initialization took {} s".format(time.time()-s))
	glitches = g.get_glitch_confidence_interval((90, 100), n_glitches = N_glitches,
		srate = 4096., snr = None, seed = 0, alpha = 0.25, fhigh = 1024, verbose = True)
	
	assert glitches.shape[0]==N_glitches
	

if __name__ == '__main__':
	test_import()
	test_basics()
	test_snr()
	test_confidence_interval()


























	
