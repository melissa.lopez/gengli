.. mbank documentation master file

Welcome to ``gengli``'s documentation!
======================================

.. automodule:: gengli
   :members: 

.. toctree::
   :maxdepth: 2
   :caption: Package instructions
   :name: usage

   usage/install.md
   usage/overview.md
   usage/validation.md
   usage/training.md
   usage/review.md

.. toctree::
   :maxdepth: 2
   :caption: Package reference
   :name: package_reference
   
   package_reference/glitch_generator.rst
   package_reference/utils.rst
   package_reference/ctgan.rst
 
.. toctree::
   :maxdepth: 2
   :caption: About
   
   about.md
  
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
